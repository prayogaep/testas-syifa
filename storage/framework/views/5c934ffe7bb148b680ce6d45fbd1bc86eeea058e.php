<?php $__env->startSection('content'); ?>
<div class="container my-2">
    <h2>Show Kategori <?php echo e($kategori->id); ?></h2>
    <h4><?php echo e($kategori->nama); ?></h4>

    <a href="/kategori" class="btn btn-primary">Kembali</a>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testas-syifa\resources\views/halaman/kategori/show.blade.php ENDPATH**/ ?>