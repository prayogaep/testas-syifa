<?php $__env->startSection('content'); ?>
<div class="container my-2">
    <h2><?php echo e($berita->judul); ?></h2>
    <h4>Kategori : <?php echo e($berita->kategori->nama); ?></h4>
    <h4>Penulis : <?php echo e($berita->user->name); ?></h4>
    <p><?php echo e($berita->isi); ?></p>
    <hr>
    <h2>Komentar dan Masukan</h2>
    <?php $__empty_1 = true; $__currentLoopData = $berita->komentar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <?php echo e($item->user->name); ?>

                </div>
                <div class="col-md-8">
                    <?php echo e($item->isi); ?>

                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        Tidak Ada Komentar
    <?php endif; ?>
    <form action="/komentar" method="post" class="my-3">
        <?php echo csrf_field(); ?>
        <input type="hidden" value="<?php echo e($berita->id); ?>" name="berita_id">
        <textarea name="isi" class="form-control" id="isi" cols="30" rows="10"></textarea>
        <button type="submit" class="btn btn-primary my-2">Kirim Komentar</button>
    </form>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testas-syifa\resources\views/halaman/berita/show.blade.php ENDPATH**/ ?>