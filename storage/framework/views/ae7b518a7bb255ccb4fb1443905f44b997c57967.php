<?php $__env->startSection('content'); ?>
<div class="container my-2">
  <?php if(auth()->guard()->check()): ?>
  <a href="/berita/create" class="btn btn-primary my-2">Tambah Berita</a>
  <table class="table">
      <thead class="thead-light">
          <tr>
              <th scope="col">No</th>
              <th scope="col">Judul Berita</th>
              <th scope="col">Kategori</th>
              <th scope="col">Penulis</th>
              <th scope="col">Aksi</th>
            </tr>
      </thead>
      <tbody>
          <?php $__empty_1 = true; $__currentLoopData = $berita; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
          <tr>
              <td><?php echo e($key + 1); ?></th>
                <td><?php echo e($value->judul); ?></td>
                  <td><?php echo e($value->kategori->nama); ?></td>
                  <td><?php echo e($value->user->name); ?></td>
                  <td>
                      <?php if($value->user->id === Auth::user()->id): ?>
                    <form action="/berita/<?php echo e($value->id); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                      <?php echo method_field('DELETE'); ?>
                      <a href="/berita/<?php echo e($value->id); ?>" class="btn btn-primary">Read More</a>
                      <a href="/berita/<?php echo e($value->id); ?>/edit" class="btn btn-warning">Edit</a>
                      <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                      <?php else: ?>
                      <a href="/berita/<?php echo e($value->id); ?>" class="btn btn-primary">Read More</a> 
                        <?php endif; ?>
                        
                  </td>
                </tr>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                      <tr colspan="3">
                          <td>No data</td>
                        </tr>  
                      <?php endif; ?>              
                  </tbody>
                </table>
          </div>
          <?php endif; ?>
          <?php if(auth()->guard()->guest()): ?>
          <h1>Selamat Datang di portal berita kami </h1>
    <hr>
    <div class="row">
            <?php $__empty_1 = true; $__currentLoopData = $berita; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="card mx-3" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title"><?php echo e($value->judul); ?></h5>
                  <h6 class="card-subtitle mb-2 text-muted">Penulis <?php echo e($value->user->name); ?></h6>
                  <h6 class="card-subtitle mb-2 text-muted">Kategori : <?php echo e($value->kategori->nama); ?></h6>
                  <p class="card-text"><?php echo e($value->isi); ?></p>
                  <small>Waktu Posting <?php echo e($value->created_at); ?> </small><br>
                  <?php if(auth()->guard()->guest()): ?>
                  <a href="/berita/<?php echo e($value->id); ?>" class="btn btn-primary">Read More</a> 
                  <?php endif; ?>
                </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
              <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <p class="card-text">Belum Ada Berita</p>
                </div>
            </div>
            <?php endif; ?>
    </div>
    </div>
          <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testas-syifa\resources\views/halaman/berita/index.blade.php ENDPATH**/ ?>