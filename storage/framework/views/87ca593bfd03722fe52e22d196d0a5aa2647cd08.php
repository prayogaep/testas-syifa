<?php $__env->startSection('content'); ?>
<h4>Profile</h4>
<hr>
<form action="/profile/<?php echo e($profil->id); ?>" method="POST">
    <?php echo csrf_field(); ?>
    <?php echo method_field('put'); ?>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" value="<?php echo e($profil->user->email); ?>" id="email" disabled>
    </div>
    <div class="form-group">
        <label for="name">Nama</label>
        <input type="text" class="form-control" name="name" value="<?php echo e($profil->user->name); ?>" id="name" disabled>
    </div>
    <div class="form-group">
        <label for="Umur">Umur</label>
        <input type="number" class="form-control" name="umur" value="<?php echo e($profil->umur); ?>" id="umur">
    </div>
    <div class="form-group">
        <label for="Alamat">Alamat</label>
        <textarea name="alamat" class="form-control" cols="30" rows="10"><?php echo e($profil->alamat); ?></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testas-syifa\resources\views/halaman/profile/index.blade.php ENDPATH**/ ?>