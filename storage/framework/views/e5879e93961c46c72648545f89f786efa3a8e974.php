<?php $__env->startSection('content'); ?>
    <div class="container my-2">
<a href="<?php echo e(route('kategori.create')); ?>" class="btn btn-primary mb-3">Tambah Kategori</a>
<table class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Kategori</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $__empty_1 = true; $__currentLoopData = $kategori; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <tr>
            <td><?php echo e($key + 1); ?></th>
                <td><?php echo e($value->nama); ?></td>
                <td>
                    <form action="<?php echo e(route ('kategori.destroy', ['kategori'=>$value->id])); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('DELETE'); ?>
                        <a href="<?php echo e(route ('kategori.show', ['kategori'=> $value->id])); ?>" class="btn btn-info">Show</a>
                        <a href="<?php echo e(route ('kategori.edit', ['kategori'=> $value->id])); ?>" class="btn btn-primary">Edit</a>
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                    <?php endif; ?>              
                </tbody>
            </table>
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testas-syifa\resources\views/halaman/kategori/index.blade.php ENDPATH**/ ?>